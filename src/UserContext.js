import React from 'react';

// creates a context object
const UserContext = React.createContext();

// creates a "Provider" component which allows other components to consume/use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;