import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import userContext from '../userContext'; 
import swal from 'sweetalert2'

export default function Register() {

	const {user} = useContext(userContext)

	const navigate = useNavigate();

	// State Hooks -> stores value of the input fields.
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false)

	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	useEffect(() => {
		// validation to enable the register button when all fields are populated and both password match.
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length == 11)) {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	});

	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
			})
		})
    	.then(res => res.json())
    	.then(data => {

	    	// console.log(data);

    		if(data) {

				swal.fire({
					title: 'Registration Failed',
					icon: 'error',
					text: 'Email already exists. Please try again!'
				})
			}
			else {

				fetch('http://localhost:4000/users/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
					body: JSON.stringify({
						firstName : firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)

					swal.fire({
						title: 'Registration Successful',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					})

					navigate('/login');
		    	})
				
			}
		})

		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNumber('');
		setPassword1('');
		setPassword2('');

		// alert('Thank you for registering!')
	}

	return (
		(user.id !== null) ? <Navigate to="/courses"/> :
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group className="mb-3" controlId="userFistName">
			    	<Form.Label>First Name</Form.Label>
			        <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="userLastName">
			    	<Form.Label>Last Name</Form.Label>
			        <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="userEmail">
			    	<Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
			        <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="userContact">
			    	<Form.Label>Mobile Number</Form.Label>
			        <Form.Control type="text" placeholder="Enter Mobile Number" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)} required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Confirm Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
			    </Form.Group>
			    
			    {/* Conditional Rendering -> If the button is active, it should be clickable, else it should be unclickable*/}
			    {
			    	isActive ? <Button variant="primary" type="submit" id="submitBtn">Register</Button> : <Button variant="primary" type="submit" id="submitBtn" disabled>Register</Button>
			    }
		    </Form>
	)
}